#!/bin/bash
# Print out BTS commands to fix typos and other usertags issues
# CRON=46 19 * * *

set -e

print_bts_usertags_cmdline () {
	bad="$(basename "$2")"
	good="$1"
	echo -n "bts user $bad , "
	for tag in $(sed --quiet "s/Tag: //p" "$2") ; do
		for bug in $(sed --quiet "/^Tag: $tag/,/^$/{/^Tag: /d;p}" "$2" | grep --only-matching '[0-9]*') ; do
			echo -n "usertags $bug - $tag , "
		done
	done
	echo -n "user $good , "
	for tag in $(sed --quiet "s/Tag: //p" "$2") ; do
		for bug in $(sed --quiet "/^Tag: $tag/,/^$/{/^Tag:/d;p}" "$2" | grep --only-matching '[0-9]*') ; do
			echo -n "usertags $bug + $tag , "
		done
	done
	echo
}

find_and_correct () {
	local f
	test -d "$1" || return 0
	find "$1/" -type f | while read -r f ; do
		print_bts_usertags_cmdline "$(basename "$f" | sed "$2")" "$f"
	done
}

copy () {
	dir="fixes/$2/"
	mkdir --parents "$dir"
	find all -type f -wholename "all/*/$1" -print0 |
	xargs --null --no-run-if-empty cp --target-directory "$dir"
}

scratchdir=/srv/scratch/qa.debian.org/usertags
mkdir --parents "$scratchdir"
cd "$scratchdir"
rsync --timeout=60 --recursive --delete rsync://bugs-mirror.debian.org/bts-spool-index/user/ all/
# tagsregex=$(ssh buxtehude.debian.org "sed '/^1;$/i print join \"|\", @gTags;' < /srv/bugs.debian.org/etc/config | perl")
#tagsregex="patch|wontfix|moreinfo|unreproducible|fixed|potato|woody|sid|help|security|upstream|pending|sarge|sarge-ignore|experimental|d-i|confirmed|ipv6|lfs|fixed-in-experimental|fixed-upstream|l10n|newcomer|etch|etch-ignore|lenny|lenny-ignore|squeeze|squeeze-ignore|wheezy|wheezy-ignore|jessie|jessie-ignore|stretch|stretch-ignore|buster|buster-ignore|bullseye|bullseye-ignore"
#grep -rE "^Tag: ($tagsregex)$" all || true
rm --recursive --force fixes
mkdir fixes
copy '*.or' or
copy '*.ort' ort
copy '*.ogr' ogr
copy '*.rog' rog
copy '*.orge' orge
copy '*.orrg' orrg
copy '*.d.o' d.o
copy '*.d.n' d.n
copy '*debia[.-]*' debia
copy '*debiarn*' debiarn
copy '*dbeian*' dbeian
copy '*debain*' debain
copy '*deban*' deban
copy '*.debian' debian
copy '*.debian-org' debian-org
copy '*.debian-net' debian-net
copy '*.debian-com' debian-com
#copy '*.debian.net' debian.net
copy '*.debian.com' debian.com
copy '*packages.qa.debian.org' packages.qa
copy '*packge*.debian.org' packge
copy '*package.debian.org' package
copy '*list.debian.org' list
copy '*%40alioth.debian.org' alioth
copy '*.debian.org*bugs.debian.org' qa
copy 'debian-m68k%40lists.debian.org' 68k
copy 'debian-s390x%40lists.debian.org' s390
copy 'debian-power%40lists.debian.org' power
copy 'debian-ppc%40lists.debian.org' ppc
copy '*mulitarch*' mulitarch
(
cd fixes
find . -type f -print0 | xargs --null --no-run-if-empty rename 's/%40/@/'
find . -empty -delete
find_and_correct or 's/\.or$/.org/'
find_and_correct ort 's/\.ort$/.org/'
find_and_correct ogr 's/\.ogr$/.org/'
find_and_correct rog 's/\.rog$/.org/'
find_and_correct orge 's/\.orge$/.org/'
find_and_correct orrg 's/\.orrg$/.org/'
find_and_correct d.o 's/\.d\.o$/.debian.org/'
find_and_correct d.n 's/\.d\.n$/.debian.net/'
find_and_correct debiarn 's/debiarn/debian/g'
find_and_correct dbeian 's/dbeian/debian/g'
find_and_correct debain 's/debain/debian/g'
find_and_correct debia 's/debia\([-.]\)/debian\1/g'
find_and_correct deban 's/deban/debian/g'
find_and_correct debian 's/\.debian$/&.org/'
find_and_correct debian-org 's/debian-org/debian.org/'
find_and_correct debian-net 's/debian-net/debian.net/'
find_and_correct debian-com 's/debian-com/debian.com/'
find_and_correct debian.com 's/\.debian\.com$/.debian.org/'
#find_and_correct debian.net 's/\.debian\.net$/.debian.org/'
find_and_correct packages.qa 's/packages\.qa\.debian\.org$/packages.debian.org/'
find_and_correct package 's/package\.debian\.org$/packages.debian.org/'
find_and_correct packge 's/packges\?\.debian\.org$/packages.debian.org/'
find_and_correct list 's/list\.debian\.org$/lists.debian.org/'
find_and_correct qa 's/\.debian\.org@bugs\.debian\.org$/.debian.org@packages.debian.org/'
find_and_correct alioth 's/@alioth\.debian\.org$/@lists.alioth.debian.org/'
find_and_correct 68k 's/m68k/68k/g'
find_and_correct s390 's/s390x/s390/g'
find_and_correct power 's/power@/powerpc@/'
find_and_correct ppc 's/ppc/powerpc/g'
find_and_correct mulitarch 's/mulitarch/multiarch/g'
)
rm -rf fixes
