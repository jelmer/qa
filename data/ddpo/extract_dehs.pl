#!/usr/bin/perl -w

# Copyright (C) 2011 Christoph Berg <myon@debian.org>
# Copyright (C) 2012 Bart Martens <bartm@knars.be>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use Dpkg::Version;
use DBD::Pg;
use DB_File;

my %arch;
my $count = 0;

foreach my $dist ( "unstable", "experimental" )
{
	my $package = undef;
	my $version = undef;

	open INPUT, "xzcat /srv/mirrors/debian/dists/$dist/*/source/Sources.xz |" or die "failed to read $dist Sources files";
	while(<INPUT>)
	{
		chomp;

		$package = $1 if( /^Package: (.*)/ );
		$version = $1 if( /^Version: (.*)/ );

		if( /^$/ )
		{
			die if( ! defined $package );
			die if( ! defined $version );

			$count++ if( not defined $arch{$package}{$dist} );
			$arch{$dist}{$package} = $version
				if( ! defined $arch{$dist}{$package}
				or Dpkg::Version::version_compare( $arch{$dist}{$package}, $version ) < 0 );

			$package = undef;
			$version = undef;
		}
	}
}

die "low number of packages in Sources files: $count" if( $count < 20000 ); # 20323 on 2012-12-02

my $db_filename = "dehs-new.db";
my %db;

my $dbfile = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
	or die "Can't open database $db_filename : $!";

my $qadb = DBI->connect ("dbi:Pg:service=qa", "", "");
$qadb->do ("SET client_encoding = 'utf-8'");

# hack: there are several keys DEBIAN/....\x011234..., use them in order
my $query = "select * from mole.watch where key like E'%\x01%' and value <> '' order by regexp_matches(key, E'\\\\d+\$')";
my $sth = $qadb->prepare ($query);
$sth->execute ();

sub strip_epoch
{
	my $version = shift;
	$version =~ s/^\d+://;
	return $version;
}

my $timestamp_too_old = time - 60*60*24*40;

while (my ($pkg, $xml) = $sth->fetchrow_array) {

	# key must match pattern to extract package and version
	next if( $pkg !~ m!.*/([^/_]+)_([^/_]+)\.dsc.(\d+)$! );
	my $package = $1;
	my $version = $2;
	my $timestamp = $3;

	# only interested in what's currently in unstable and experimental
	next if( ( not defined $arch{"unstable"}{$package} or strip_epoch( $arch{"unstable"}{$package} ) ne $version )
		and ( not defined $arch{"experimental"}{$package} or strip_epoch( $arch{"experimental"}{$package} ) ne $version ) );

	# delete old data if any
	delete $db{"${package}_$version:status"};
	delete $db{"${package}_$version:upstream-version"};

	# don't use old uscan results
	next if( $timestamp <= $timestamp_too_old );

	# look for status and upstream-version
	my $status = undef;
	my $upstream_version = undef;
	while( $xml =~ /<(.+)>(.*)</g )
	{
		$status = $2 if( $1 eq "status" );
		$upstream_version = $2 if( $1 eq "upstream-version" );
	}

	# not interested in bad results
	next if( defined $upstream_version and $upstream_version !~ /^\d/ and $version =~ /^\d/ );

	# register results if available
	$db{"${package}_$version:status"} = $status if( defined $status );
	$db{"${package}_$version:upstream-version"} = $upstream_version if( defined $upstream_version );
}
