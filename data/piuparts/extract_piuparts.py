#!/usr/bin/python3
#
# Copyright (C) 2014 David Steele (dsteele@gmail.com), (C) 2018 Asheesh Laroia
#
# This file is part of the Debian Developer's Package Overview (DDPO)
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

# This extracts the information from piuparts for each package

import textwrap
import argparse
import os
import sys
import json
import dbm.ndbm
from collections import defaultdict


DBFILE = 'piuparts-new'

SUMMID = "Piuparts Package Test Results Summary"
SUMMVER = "1."
DEFSEC = 'overall'

class SummaryException(Exception):
    pass


dist_sort_order = defaultdict(lambda: 100, [
                                           ('oldstable', 10),
                                           ('stable', 20),
                                           ('testing', 30),
                                           ('unstable', 40),
                                           ('experimental', 50),
                                           ])


flag_description = defaultdict(lambda: 'Unknown', [
                                           ('P', 'Passed'),
                                           ('X', 'Blocked'),
                                           ('W', 'Waiting'),
                                           ('F', 'Failed'),
                                           ('-', 'Unknown'),
                                           ])

def tooltip(summary, pkg):
    """Returns e.g. "Failed in stable and testing, blocking 5 packages"."""

    tip = ''
    pkgdict = summary['packages']

    if pkg in pkgdict:
        flag, block_cnt, url = pkgdict[pkg][DEFSEC][0:3]

        if len(flag) != 1 or not isinstance(block_cnt, int) \
                or not url.startswith('http'):
            err = "Invalid entry - "
            err += ', '.join([x.__str__() for x in (flag, block_cnt, url)])
            raise SummaryException(err)

        dists = [x for x in pkgdict[pkg] if x != DEFSEC
                                         and pkgdict[pkg][x][0] == flag]
        dists = sorted(dists, key=lambda x: dist_sort_order[x])

        tip = flag_description[flag]

        if len(dists) > 2:
            tip += ' in ' + ', '.join(dists[:-1]) + ' and ' + dists[-1]
        elif len(dists) == 2:
            tip += ' in ' + ' and '.join(dists)
        elif len(dists) == 1:
            tip += ' in ' + dists[0]

        if block_cnt:
            tip += ", blocking %d package" % block_cnt
            if block_cnt > 1:
                tip += "s"

    return tip

def summary_read(fname):
    with open(fname, 'r') as fl:
        result = json.load(fl)

    if result["_id"] != SUMMID or not result["_version"].startswith(SUMMVER):
        raise SummaryException('Summary JSON header mismatch')

    return result

class MyParser(argparse.ArgumentParser):
   def error(self, message):
      sys.stderr.write('error: %s\n' % message)
      self.print_help()
      sys.exit(2)

if __name__ == "__main__":

    parser = MyParser(description="Parse piuparts json to db",
                       epilog=textwrap.fill("""\
                              Parse the "%s" file (version %s), and convert
                              the contents into a Berkley database file,
                              keyed off of the package name, and containing
                              a space-separated list of the extracted
                              parameters.
                              """ % (SUMMID, SUMMVER)))

    parser.add_argument('summfile', metavar='summaryfile',
                        help="Piuparts summary JSON file, as input (e.g. piuparts-summary.json)")

    parser.add_argument('outfile',
                        nargs='?',
                        default=DBFILE,
                        help='The output Berkley DB file (default %s(.db))' % DBFILE,
                        metavar='dbpath',
                        )

    args = parser.parse_args()

    summary = summary_read(args.summfile)
    db = dbm.ndbm.open(args.outfile, 'n')

    for pkg in summary['packages']:
        flag, blocked, url = summary['packages'][pkg][DEFSEC][0:3]
        tip = tooltip(summary, pkg)

        db[pkg] = " ".join([flag, url, tip])

    db.close()

# vim:et:sw=4:
